import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import * as firebase from 'firebase';

@Injectable()
export class UserService implements CanActivate{
    userLoggedIn: boolean = false;
    loggedInUser: string;
    authUser: any;
    
    constructor( private _router: Router){
        //firebase settings
        firebase.initializeApp({
            apiKey: "AIzaSyAFeGaPuarTwzDvMhmwYbQ-sVsOEaSY_w0",
            authDomain: "first-fire-1932f.firebaseapp.com",
            databaseURL: "https://first-fire-1932f.firebaseio.com",
            projectId: "first-fire-1932f",
            storageBucket: "first-fire-1932f.appspot.com",
            messagingSenderId: "836941445926"
        })
    }

    canActivate( route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        let url: string = state.url;
        return this.verifyLogin(url);
    }

    verifyLogin(url: string) : boolean{
        if (this.userLoggedIn){ return true; }

        this._router.navigate(['/admin/login']);
        return false;
    }

    register(email: string, password: string){
        firebase.auth().createUserWithEmailAndPassword(email, password)
        .catch(function(error){
            alert(`${error.message} Please try again!`);
        });
    }

    verifyUser(){
        
        this.authUser = firebase.auth().currentUser;

        if(this.authUser){
            //alert(`Welcome ${this.authUser.email}`);
            this.loggedInUser = this.authUser.email;
            this.userLoggedIn = true;
            this._router.navigate(['./admin']);
        }
    }

    login(loginEmail: string, loginPassword: string){
        firebase.auth().signInWithEmailAndPassword(loginEmail, loginPassword)
        .catch(function(error){
            alert(`${error.message} Unable to login. Please try again!`);
        })
    }

    logout(){
        this.userLoggedIn = false;
        firebase.auth().signOut()
        .then(function(error){
            alert(`Logged Out!`);
        },function(error){
            alert(`${error.message} Unable to logout. Please try again!`);
        })
    }
}