import { Component } from '@angular/core';
import { UserService } from '../adminShared/user.service';
import { Router } from '@angular/router';

@Component({
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})

export class LoginComponent{
    email: string;
    password1: string;

    constructor( private _userSVC: UserService, private _router: Router){}

    login(){
        this._userSVC.login(this.email, this.password1);
        this._userSVC.verifyUser();
    }

    signup(){
        this._router.navigate(['/admin/signup']);
    }

    cancel(){
        this._router.navigate([''])
    }
}